
var surveyApp = angular.module('surveyApp', [
    'ui.router',
    'simplePagination',
    'chart.js'
]);

surveyApp.config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
    function($urlRouterProvider, $stateProvider, $httpProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'partials/home.html'
        })
        .state('survey_form', {
            url: '/survey_form',
            templateUrl: 'partials/survey_form.html',
            controller: 'SurveyFormCtrl'
        })
        .state('survey_list', {
            url: '/survey_list',
            templateUrl: 'partials/survey_list.html',
            controller: 'SurveyListCtrl'
        })
        .state('survey_summary', {
            url: '/survey_summary',
            templateUrl: 'partials/survey_summary.html',
            controller: 'SurveySummary'
        });
}]);

surveyApp.run(['$state','SurveyService', function ($state, SurveyService) {
    $state.transitionTo('home');
    SurveyService.getCollection();
}]);
