/**
 * Created by elvin on 1/12/2016.
 */
surveyApp.service('SurveyService', function(){

    this.collection = [];

    this.getCollection = function(){
        this.collection = JSON.parse(window.localStorage.getItem("collection"));
    };

    this.updateCollection = function(survey_list){
        window.localStorage.setItem("collection",JSON.stringify(survey_list));
        this.collection = JSON.parse(window.localStorage.getItem("collection"));
    };

});
