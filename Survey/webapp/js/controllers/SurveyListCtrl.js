surveyApp.controller('SurveyListCtrl', ['$scope','Pagination', 'SurveyService', function ($scope, Pagination, SurveyService) {

    $scope.pagination = Pagination.getNew(5);
    $scope.modalShown = false;
    $scope.survey_detail = [];
    $scope.$watch(function(){
        return SurveyService.collection;
    }, function(newValue, oldValue){
        $scope.survey_list = SurveyService.collection;
        $scope.pagination.numPages = Math.ceil(_.size($scope.survey_list)/$scope.pagination.perPage);
    });

    $scope.showDetail = function(index, pageNumber){
        var surveyIndex = "";
        if(pageNumber == 0){
            surveyIndex = index;
        }else{
            surveyIndex = index + (5*pageNumber)
        }
        $scope.survey_detail = $scope.survey_list[surveyIndex];
        $scope.modalShown = !$scope.modalShown;
    };
    if($scope.survey_list != null){
        $scope.pagination = Pagination.getNew();
        $scope.pagination.numPages = Math.ceil($scope.survey_list.length/$scope.pagination.perPage);
    }

}]);