surveyApp.controller('SurveySummary', ['$scope', 'SurveyService', function ($scope, SurveyService) {

    $scope.summary = true;
    $scope.survey_list = SurveyService.collection;
    $scope.colors = ['Red','Blue','Green','Yellow','Pink'];
    $scope.colorSummary = [];
    $scope.labels = [];
    $scope.data = [];
    $scope.chartColor = [];

    for(var i = 0; i < $scope.colors.length; i++){
        var count = _.countBy($scope.survey_list, {color:$scope.colors[i]}).true;
        if(count != 0 && count != undefined){
            $scope.data.push(count);
            $scope.labels.push($scope.colors[i]);
            $scope.chartColor.push(test($scope.colors[i]));
            $scope.colorSummary.push({color:$scope.colors[i], count:count});
        }
    }
    function test(color){
        var hex = '';
        switch(color){
            case 'Red':
                hex = '#FF0000';
                break;
            case 'Blue':
                hex = '#0000FF';
                break;
            case 'Green':
                hex = '#00FF00';
                break;
            case 'Yellow':
                hex = '#FFFF00';
                break;
            case 'Pink':
                hex = '#FF00FF';
                break;
        }
        return hex;
    }

    $scope.view = function(){
        $scope.summary = !$scope.summary;
    }


}]);