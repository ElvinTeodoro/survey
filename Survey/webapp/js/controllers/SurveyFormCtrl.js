surveyApp.controller('SurveyFormCtrl', ['$scope', '$filter', 'SurveyService', function ($scope, $filter, SurveyService) {

    $scope.survey_list = [];
    if(_.size(SurveyService.collection) > 0){
        $scope.survey_list = SurveyService.collection;
    }

    $scope.survey_template = {
        name: '',
        email: '',
        gender: '',
        color: '',
        date: ''
    };

    $scope.color_template = [
        {
            name: 'Red',
            hex: '#FF0000'
        },
        {
            name: 'Blue',
            hex: '#0000FF'
        },
        {
            name: 'Green',
            hex: '#00FF00'
        },
        {
            name: 'Yellow',
            hex: '#FFFF00'
        },
        {
            name: 'Pink',
            hex: '#FF00FF'
        }
    ];

    $scope.selectedColor = "";

    $scope.$watch(function(){
        return $scope.survey_template.color;
    }, function(newValue, oldValue){
        $scope.selectedColor = _.find($scope.color_template, {name : newValue}).hex;
    });

    $scope.save = function(survey){
        survey.date = new $filter('date')(new Date(),'yyyy-MM-dd');
        $scope.survey_list.push(angular.copy(survey));
        SurveyService.updateCollection($scope.survey_list);
        $scope.clearTemplate();
    };

    $scope.clearTemplate = function(){
        $scope.survey_template = {
            name: '',
            email: '',
            gender: '',
            color: undefined,
            date: ''
        };
        $scope.selectedColor = "";
    }
}]);